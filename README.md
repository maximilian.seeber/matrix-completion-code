# Matrix Completion

This repository is meant to be complementary to my Bachelors thesis "A survey 
and comparison of different algorithms for Low-Rank Matrix Completion".
All the algorithms developed in the thesis can be found in the `matrix_completion.py` file.
For reproducibility purposes, I provide the `plotting.ipynb` notebook.
All the plots in my thesis can be reproduced with it.
Finally, in the `demo.ipynb` notebook, I provide examples for the usage of
all the algorithms.