import numpy as np
from scipy.sparse.linalg import svds

def ap(M, mask, r, epsilon=1e-9, max_iter=1000, logging='X'):
    """
    Solve the matrix completion problem with the alternating projections
    algorithm.
    Parameters:
    -----------
    M : n1 x n2 array
        matrix to complete
    mask : n1 x n2 array
        matrix with entries False (if missing) or True (if present)
    r : integer
        rank of the original matrix M
    epsilon : float
        convergence condition on the difference between iterative results
    max_iter: int
        hard limit on maximum number of iterations
    logging: String
        default 'X' logs only the matrices X_k into X_list,
        'XY' logs the matrices X_k and Y_k into X_list
    Returns:
    --------
    X: n1 x n2 array
        completed matrix
    X_list: list of n1 x n2 arrays
        a list of the iterates
    """
    X = np.copy(M*mask)
    X_list = [X]

    for _ in range(max_iter):
        X_prev = X
        U, sigma, V = svds(X, k=r)
        X = U @ np.diag(sigma) @ V
        if logging == 'XY':
            X_list += [np.copy(X)]
        X[mask] = M[mask]
        X_list += [X]
        if np.linalg.norm(X - X_prev, 'fro') < epsilon:
            break
    return X, X_list

def als(M, mask, r, lambd, epsilon=1e-9, max_iter=1000):
    """
    Solve the matrix completion problem with the alternating least
    squares algorithm.
    Parameters:
    -----------
    M : n1 x n2 array
        matrix to complete
    mask : n1 x n2 array
        matrix with entries False (if missing) or True (if present)
    r : integer
        rank of the original matrix M
    lambd: float
        regularization parameter lambda
    epsilon : float
        convergence condition on the difference between iterative results
    max_iter: int
        hard limit on maximum number of iterationst
    Returns:
    --------
    X: n1 x n2 array
        completed matrix
    X_list: list of n1 x n2 arrays
        a list of the iterates
    """
    n1, n2 = M.shape

    U = np.random.randn(r, n1)
    V = np.random.randn(r, n2)

    C_u = [np.diag(row) for row in mask]
    C_v = [np.diag(col) for col in mask.T]

    X_prev = U.T @ V
    
    X_list = [X_prev]

    for _ in range(max_iter):

        for i in range(n1):
            U[:,i] = np.linalg.solve(V @ C_u[i] @ V.T + lambd * np.eye(r), V @ C_u[i] @ M[i,:])

        for j in range(n2):
            V[:,j] = np.linalg.solve(U @ C_v[j] @ U.T + lambd * np.eye(r), U @ C_v[j] @ M[:,j])

        X = U.T @ V
        X_list += [X]
        
        if np.linalg.norm(X - X_prev, 'fro') < epsilon:
            break
        X_prev = X

    return X, X_list

def nnm(M, mask, mu, gamma, epsilon=1e-9, max_iter=1000):
    """
    Solve the matrix completion problem with the nuclear norm
    minimization algorithm.
    Parameters:
    -----------
    M : n1 x n2 array
        matrix to complete
    mask : n1 x n2 array
        matrix with entries False (if missing) or True (if present)
    mu : float
        parameter mu
    gamma: float
         parameter gamma
    epsilon : float
        convergence condition on the difference between iterative results
    max_iter: int
        hard limit on maximum number of iterationst
    Returns:
    --------
    X: n1 x n2 array
        completed matrix
    X_list: list of n1 x n2 arrays
        a list of the iterates
    """
    def ProxF(X, M, mask):
        return X + mask*(M-X)

    def ProxG(X, gamma):
        # U, sigma, V = svds(X)
        U, sigma, V = np.linalg.svd(X)
        sigma = sigma - gamma
        sigma[sigma < 0] = 0
        E = np.diag(sigma)
        return U @ E @ V

    def rProxF(X, M, mask):
        return 2*ProxF(X, M, mask) - X

    def rProxG(X, gamma):
        return 2*ProxG(X, gamma) - X

    n1, n2 = M.shape

    Y = np.zeros((n1, n2))
    Y[mask] = M[mask]

    X = np.zeros((n1, n2))

    X_list = []
    for _ in range(max_iter):
        X_prev = X
        Y = (1-mu/2)*Y + (mu/2)*rProxG(rProxF(Y, M, mask), gamma)
        X = ProxF(Y, M, mask)
        X_list += [X]
        
        if np.linalg.norm(X - X_prev, 'fro') < epsilon:
            break
    return X, X_list

def svt(M, mask, tau, delta, epsilon=1e-3, max_iter=1000):
    """
    Solve the matrix completion problem with the singular value
    threshholding algorithm.
    Parameters:
    -----------
    M : n1 x n2 array
        matrix to complete
    mask : n1 x n2 array
        matrix with entries False (if missing) or True (if present)
    tau : float
        parameter tau
    delta: float
         parameter delta
    epsilon : float
        convergence condition on the difference between iterative results
    max_iter: int
        hard limit on maximum number of iterationst
    Returns:
    --------
    X: n1 x n2 array
        completed matrix
    X_list: list of n1 x n2 arrays
        a list of the iterates
    """
    def D(X, tau):
        # U, sigma, V = svds(X)
        U, sigma, V = np.linalg.svd(X)
        sigma = sigma - tau
        sigma[sigma < 0] = 0
        E = np.diag(sigma)
        return U @ E @ V

    n1, n2 = M.shape

    Y = np.zeros((n1, n2))

    X_list = []
    for _ in range(max_iter):
        X = D(Y, tau)
        X_list += [X]
        Y = Y + delta*(mask*(M-X))
        if np.linalg.norm(mask*(X-M), 'fro') / np.linalg.norm(mask*M, 'fro') < epsilon:
            break
    return X, X_list